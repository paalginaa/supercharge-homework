package org.gina.superchargehomework.banking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoreBankingHomeworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoreBankingHomeworkApplication.class, args);
	}

}
