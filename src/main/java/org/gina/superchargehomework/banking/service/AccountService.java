package org.gina.superchargehomework.banking.service;

import org.gina.superchargehomework.banking.model.Account;
import org.gina.superchargehomework.banking.repository.AccountRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {

    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void updateAccount(Account account) {
        accountRepository.save(account);
    }

    public Account save(Account account) {
        return accountRepository.saveAndFlush(account);
    }

    public void updateMultipleAccounts(List<Account> accounts) {
        accountRepository.saveAll(accounts);
    }
}
