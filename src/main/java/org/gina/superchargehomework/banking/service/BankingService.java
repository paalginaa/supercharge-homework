package org.gina.superchargehomework.banking.service;

import lombok.extern.slf4j.Slf4j;
import org.gina.superchargehomework.banking.model.Account;
import org.gina.superchargehomework.banking.model.EventType;
import org.gina.superchargehomework.banking.model.TransactionHistory;
import org.gina.superchargehomework.banking.model.exception.RelatedEntitiesNotExistException;
import org.gina.superchargehomework.banking.model.Client;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class BankingService {

    private final ClientService clientService;
    private final AccountService accountService;
    private final TransactionHistoryService transactionHistoryService;

    public BankingService(ClientService clientService,
                          AccountService accountService,
                          TransactionHistoryService transactionHistoryService) {
        this.clientService = clientService;
        this.accountService = accountService;
        this.transactionHistoryService = transactionHistoryService;
    }

    public void doDeposit(Long clientId, int amount) throws RelatedEntitiesNotExistException {
        Client client = clientService.getClient(clientId).orElse(null);
        if (client != null) {
            Account account = client.getAccount();
            if (account == null) {
                throw new RelatedEntitiesNotExistException("Account not found for client id: " + clientId);
            }
            account.setAmount(updateAmount(amount, account, false));
            accountService.updateAccount(account);
            createTransactionHistory(client, account, amount, EventType.DEPOSIT);
        } else {
            throw new RelatedEntitiesNotExistException("Client doesn't exist with id: " + clientId);
        }
    }

    public void getMoneyFromAccount(Long clientId, int amount) throws RelatedEntitiesNotExistException {
        Client client = clientService.getClient(clientId).orElse(null);
        if (client != null) {
            Account account = client.getAccount();
            if (account == null) {
                throw new RelatedEntitiesNotExistException("Account not found for client id: " + clientId);
            }
            account.setAmount(updateAmount(amount, account, true));
            accountService.updateAccount(account);
            createTransactionHistory(client, account, amount, EventType.WITHDRAWAL);
        } else {
            throw new RelatedEntitiesNotExistException("Client doesn't exist with id: " + clientId);
        }
    }

    @Transactional
    public void transferMoney(Long sourceClientId, Long destinationClientId, int amount) throws RelatedEntitiesNotExistException {
        Client sourceClient = clientService.getClient(sourceClientId).orElse(null);
        Client destinationClient = clientService.getClient(destinationClientId).orElse(null);

        if (sourceClient != null && destinationClient != null) {
            Account sourceAccount = sourceClient.getAccount();
            Account destAccount = destinationClient.getAccount();

            if (sourceAccount == null || destAccount == null) {
                throw new RelatedEntitiesNotExistException("Account not found for client(s)");
            }

            sourceAccount.setAmount(updateAmount(amount, sourceAccount, true));
            destAccount.setAmount(updateAmount(amount, destAccount, false));

            List<Account> accountsToUpdate = addAccountsToList(sourceAccount, destAccount);
            accountService.updateMultipleAccounts(accountsToUpdate);

            createTransactionHistory(sourceClient, sourceAccount, amount, EventType.TRANSFER_SEND);
            createTransactionHistory(destinationClient, destAccount, amount, EventType.TRANSFER_RECEIVE);
        } else {
            throw new RelatedEntitiesNotExistException("Client(s) doesn't exist with id");
        }
    }

    public void printHistoryForClient(Long clientId) throws RelatedEntitiesNotExistException {
        Client client = clientService.getClient(clientId).orElse(null);
        if (client != null) {
            List<TransactionHistory> transactions = client.getTransactionHistory();
            if (!transactions.isEmpty()) {
                transactions.forEach(historyRecord -> System.out.println(historyRecord.toString()));
            } else {
                log.warn("No transaction record found for client with id: {}", clientId);
            }
        } else {
            throw new RelatedEntitiesNotExistException("Client doesn't exist with id: " + clientId);
        }

    }

    private List<Account> addAccountsToList(Account sourceAccount, Account destAccount) {
        List<Account> accountsToUpdate = new ArrayList<>();
        accountsToUpdate.add(sourceAccount);
        accountsToUpdate.add(destAccount);
        return accountsToUpdate;
    }

    private int updateAmount(int amount, Account account, boolean isWithdrawal) {
        int actualAmount = account.getAmount();
        if (isWithdrawal) {
            return actualAmount - amount;
        }
        return actualAmount + amount;
    }

    private void createTransactionHistory(Client client, Account account, int amount, EventType eventType) {
        TransactionHistory transactionHistory = new TransactionHistory();
        transactionHistory.setAmount(amount);
        transactionHistory.setClient(client);
        transactionHistory.setEventType(eventType);
        transactionHistory.setCurrentBalance(account.getAmount());
        transactionHistoryService.createRecorForTransactionHistory(transactionHistory);
    }

    public void createNewClient(String firstName, String lastName, String emailAdd) {
        Client client = new Client();
        client.setLastName(lastName);
        client.setFirstName(firstName);
        client.setEmailAddress(emailAdd);
        clientService.save(client);
        setAccountForClient(client);
    }

    private void setAccountForClient(Client client) {
        client.setAccount(createAccountForNewClient(client));
        clientService.save(client);
    }

    private Account createAccountForNewClient(Client client) {
        Account account = new Account();
        account.setAmount(0);
        account.setClient(client);
        return accountService.save(account);
    }
}
