package org.gina.superchargehomework.banking.service;

import org.gina.superchargehomework.banking.model.TransactionHistory;
import org.gina.superchargehomework.banking.repository.TransactionHistoryRepository;
import org.springframework.stereotype.Service;

@Service
public class TransactionHistoryService {

    private final TransactionHistoryRepository transactionHistoryRepository;

    public TransactionHistoryService(TransactionHistoryRepository transactionHistoryRepository) {
        this.transactionHistoryRepository = transactionHistoryRepository;
    }

    public void createRecorForTransactionHistory(TransactionHistory transactionHistory) {
        transactionHistoryRepository.save(transactionHistory);
    }
}
