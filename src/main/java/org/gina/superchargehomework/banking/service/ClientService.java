package org.gina.superchargehomework.banking.service;

import org.gina.superchargehomework.banking.model.Client;
import org.gina.superchargehomework.banking.repository.ClientRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientService {

    private final ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public Optional<Client> getClient(Long id) {
       return clientRepository.findById(id);
    }

    public void save(Client client) {
        clientRepository.saveAndFlush(client);
    }
}
