package org.gina.superchargehomework.banking.controller;

import lombok.extern.slf4j.Slf4j;
import org.gina.superchargehomework.banking.model.exception.RelatedEntitiesNotExistException;
import org.gina.superchargehomework.banking.service.BankingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class BankingEventsController {

    private final BankingService bankingService;

    @Autowired
    public BankingEventsController(BankingService bankingService) {
        this.bankingService = bankingService;
    }

    @PostMapping("/register/{firstName}/{lastName}/{emailAdd}")
    public void registerNewClient(@PathVariable String firstName,
                                  @PathVariable String lastName,
                                  @PathVariable String emailAdd) {
        bankingService.createNewClient(firstName, lastName, emailAdd);
    }

    @PostMapping("/deposit/{clientId}/{amount}")
    public void doDeposit(@PathVariable Long clientId,
                          @PathVariable int amount) {
        try {
            bankingService.doDeposit(clientId, amount);
        } catch (RelatedEntitiesNotExistException e) {
            log.error("Client doesn't exist with id: " + clientId, e);
        }
    }

    @PostMapping("/withdrawal/{clientId}/{amount}")
    public void getMyMoney(@PathVariable Long clientId,
                           @PathVariable int amount) {
        try {
            bankingService.getMoneyFromAccount(clientId, amount);
        } catch (RelatedEntitiesNotExistException e) {
            log.error("Client doesn't exist with id: " + clientId, e);
        }
    }

    @PostMapping("/transaction/{sourceClient}/{destinationClient}/{amount}")
    public void transaction(@PathVariable Long sourceClient,
                            @PathVariable Long destinationClient,
                            @PathVariable int amount) {
        try {
            bankingService.transferMoney(sourceClient, destinationClient, amount);
        } catch (RelatedEntitiesNotExistException e) {
            log.error("Related client(s) or account(s) missing!", e);
        }
    }

    @PostMapping("/history/{clientId}")
    public void printHistory(@PathVariable Long clientId) {
        try {
            bankingService.printHistoryForClient(clientId);
        } catch (RelatedEntitiesNotExistException e) {
            log.error("Client is missing");
        }
    }
}
