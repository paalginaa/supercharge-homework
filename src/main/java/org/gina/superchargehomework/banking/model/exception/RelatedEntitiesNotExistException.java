package org.gina.superchargehomework.banking.model.exception;

public class RelatedEntitiesNotExistException extends Throwable {
    public RelatedEntitiesNotExistException(String errorMessage) {
        super(errorMessage);
    }
}
