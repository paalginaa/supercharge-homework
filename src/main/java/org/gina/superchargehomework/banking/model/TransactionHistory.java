package org.gina.superchargehomework.banking.model;

import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;

@Entity
@Setter
public class TransactionHistory {

    public TransactionHistory() {
        this.localDateTime = LocalDateTime.now();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime localDateTime;

    @ManyToOne
    private Client client;

    @Enumerated(EnumType.STRING)
    private EventType eventType;

    private int amount;

    private int currentBalance;

    @Override
    public String toString() {
        return "TransactionHistory{" +
                "id=" + id +
                ", localDateTime=" + localDateTime +
                ", client=" + client +
                ", eventType=" + eventType +
                ", amount=" + amount +
                ", currentBalance=" + currentBalance +
                '}';
    }
}
