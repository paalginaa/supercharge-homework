package org.gina.superchargehomework.banking.model;

public enum EventType {

    TRANSFER_SEND, TRANSFER_RECEIVE, DEPOSIT, WITHDRAWAL
}