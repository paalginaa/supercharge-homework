package org.gina.superchargehomework.banking.service;

import org.gina.superchargehomework.banking.model.Account;
import org.gina.superchargehomework.banking.model.Client;
import org.gina.superchargehomework.banking.model.TransactionHistory;
import org.gina.superchargehomework.banking.model.exception.RelatedEntitiesNotExistException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BankingServiceTest {

    @Mock
    private ClientService clientService;

    @Mock
    private AccountService accountService;

    @Mock
    private TransactionHistoryService transactionHistoryService;

    private BankingService bankingService;

    @Before
    public void setUp() {
        bankingService = new BankingService(clientService, accountService, transactionHistoryService);
    }

    @Test
    public void testDoDeposit() throws RelatedEntitiesNotExistException {
        Client client = getClient();
        when(clientService.getClient(1L)).thenReturn(Optional.of(client));
        bankingService.doDeposit(1L, 500);
        assertEquals(1500, client.getAccount().getAmount());
    }

    @Test(expected = RelatedEntitiesNotExistException.class)
    public void testNoClient() throws RelatedEntitiesNotExistException {
        when(clientService.getClient(1L)).thenReturn(Optional.empty());
        bankingService.doDeposit(1L, 500);
    }

    @Test(expected = RelatedEntitiesNotExistException.class)
    public void testNoAccount() throws RelatedEntitiesNotExistException {
        Client client = getClientWithoutAccount();
        when(clientService.getClient(1L)).thenReturn(Optional.of(client));
        bankingService.doDeposit(1L, 500);
    }

    @Test
    public void testGetMoney() throws RelatedEntitiesNotExistException {
        Client client = getClient();
        when(clientService.getClient(1L)).thenReturn(Optional.of(client));
        bankingService.getMoneyFromAccount(1L, 500);
        assertEquals(500, client.getAccount().getAmount());
    }

    @Test(expected = RelatedEntitiesNotExistException.class)
    public void testNoClientForWithdrawal() throws RelatedEntitiesNotExistException {
        when(clientService.getClient(1L)).thenReturn(Optional.empty());
        bankingService.getMoneyFromAccount(1L, 500);
    }

    @Test(expected = RelatedEntitiesNotExistException.class)
    public void testNoAccountForWithdrawal() throws RelatedEntitiesNotExistException {
        Client client = getClientWithoutAccount();
        when(clientService.getClient(1L)).thenReturn(Optional.of(client));
        bankingService.getMoneyFromAccount(1L, 500);
    }

    @Test
    public void testTransfer() throws RelatedEntitiesNotExistException {
        Client client = getClient(1L);
        Client client2 = getClient(2L);
        when(clientService.getClient(1L)).thenReturn(Optional.of(client));
        when(clientService.getClient(2L)).thenReturn(Optional.of(client2));
        bankingService.transferMoney(1L, 2L, 200);

        assertEquals(800, client.getAccount().getAmount());
        assertEquals(1200, client2.getAccount().getAmount());
    }

    private Client getClient(long id) {
        Client client = new Client();
        client.setId(id);
        client.setAccount(getAccount(client));
        return client;
    }

    private Client getClientWithoutAccount() {
        Client client = new Client();
        client.setId(1L);
        return client;
    }

    private Client getClient() {
        Client client = new Client();
        client.setId(1L);
        client.setAccount(getAccount(client));
        return client;
    }

    private Account getAccount(Client client) {
        Account account = new Account();
        account.setAmount(1000);
        account.setClient(client);
        return account;
    }


}